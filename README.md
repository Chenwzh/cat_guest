# 猫客

#### 介绍
安卓项目


#### 使用说明

1. 启动app，首先展示的是应用首界面，可选择登录或注册
2. 点击注册，可进入注册页面，可进入登录界面
3. 登录后，首先可看到自己关注的用户的最新的博客
4. 点击推荐，可随机推荐用户
5. 点击其他用户的图标，可进入其他用户的个人主页
6. 点击最新，可看到所有用户的最新的博客
7. 点击个人图标，可进入个人主页
8. 点击左上角菜单，可弹出菜单栏
9. 点击菜单的设置，可修改个人信息
10. 点击正下方的创建博客，可新建博客，并且可选择图片和拍照
11. 在其他用户主页，可进行关注和取消关注

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)