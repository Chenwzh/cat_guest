const express = require('express')
const app = express()
const db = require('../apis/connectMongo')


let userFollowSchema = new db.Schema({
    username:       {type: String},
    followUser:     {type: String}
}, {collection: 'userFollow'})       

let userFollowModel = db.model('userFollowModel', userFollowSchema)

module.exports = {
  //  创建记录
  createFollow: async (username, followUser) => {
    try {
      await userFollowModel.create({
        'username':     username,  
        'followUser':   followUser
      })
    } catch (err) {
      console.log(err)
    }
  },
  deleteFollow: async (username, followUser) => {
    try {
      await userFollowModel.deleteOne({
        'username':     username,  
        'followUser':   followUser
      })
    } catch (err) {
      console.log(err)
    }
  },
  getFollow: async (username) => {
    try {
        let docs = await userFollowModel.find({
          'username':   username
        })
        return docs
    } catch (err) {
        console.log(err)
    }
  }
}