const express = require('express')
const app = express()
const db = require('../apis/connectMongo')

let albumSchema = new db.Schema({
    username:     {type: String},
    graph:        {type: String}
}, {collection: 'album'})       

let albumModel = db.model('albumModel', albumSchema)

module.exports = {
  //  创建记录
  createGraph: async (username, graph) => {
    try {
      await albumModel.create({
        'username': username,
        'graph':    graph
      })
    } catch (err) {
      console.log(err)
    }
  },
  //  根据用户名查找记录
  findByUsername: async (username) => {
    try {
      let docs = await albumModel.find({'username': username}).toArray(function(err,result){})
      return docs
    } catch (err) {
      console.log(err)
    }
  }
}