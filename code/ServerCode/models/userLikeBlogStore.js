const express = require('express')
const app = express()
const db = require('../apis/connectMongo')

let userLikeBlogSchema = new db.Schema({
    name:         {type: String},
    author:       {type: String},
    content:      {type: String}
}, {collection: 'userLikeBlog'})       

let userLikeBlogModel = db.model('userLikeBlogModel', userLikeBlogSchema)

module.exports = {
  //  添加喜欢的博客
  createLikeBlog: async (name, author, content) => {
    try {
      await userLikeBlogModel.create({
        'name':     name,  
        'author':   author,
        'content':  content 
      })
    } catch (err) {
      console.log(err)
    }
  },
  //  删除喜欢的博客
  deleteLikeBlog: async (name, author, content) => {
    try {
        await userLikeBlogModel.deleteOne({
          'name':   name,
          'author': author,
          'content':content
        })
      } catch (err) {
        console.log(err)
    }
  },
  //  获取喜欢的博客
  getLikeBlog: async (name) => {
    try {
        let docs = await userLikeBlogModel.find({
          'name':   name
        })
        return docs
    } catch (err) {
        console.log(err)
    }
  }
}