const express = require('express')
const app = express()
const db = require('../apis/connectMongo')

let blogCommentSchema = new db.Schema({
    author:       {type: String},       //  博客作者
    content:      {type: String},       //  博客内容
    commentor:    {type: String},       //  评论者
    comment:      {type: String},       //  评论内容
    dateTime:     {type: String}        //  评论时间
}, {collection: 'blogComment'})       

let blogCommentModel = db.model('blogCommentModel', blogCommentSchema)

module.exports = {

}