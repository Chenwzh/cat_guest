const express = require('express')
const app = express()
const db = require('../apis/connectMongo')

let blogGraphSchema = new db.Schema({
    author:       {type: String},
    content:      {type: String},
    graph:        {type: String}
}, {collection: 'blogGraph'})       

let blogGraphModel = db.model('blogGraphModel', blogGraphSchema)

module.exports = {
  //  创建记录
  createGraph: async (author, content, graph) => {
    try {
      await blogGraphModel.create({
        'author':   author,  
        'content':  content,
        'graph':    graph
      })
    } catch (err) {
      console.log(err)
    }
  },
  findGraph: async (author, content) => {
    try {
      let docs = await blogGraphModel.find({
        'author':   author,  
        'content':  content
      })
      return docs
    } catch (err) {
      console.log(err)
    }
  }
}