const express = require('express')
const router = express.Router()
const userStore = require('../models/userStore')


router.post('/', async (req, res, next) => {
    res.status(200)
    res.type('application/json')
  
    let docs = await userStore.findByUsername(req.body.username)
    if (docs) {
        res.send({
            'status': false,
            'message': '用户名已存在'          
        })
        return
    }

    docs = await userStore.findByName(req.body.name)
    if (docs) {
        res.send({
            'status': false,
            'message': '昵称已存在'          
        })
        return
    }


    await userStore.createUser(req.body.name, req.body.username, req.body.password, req.body.icon)
    res.send({
        'status': true,
        'message': '注册成功'
    })
})

module.exports = router