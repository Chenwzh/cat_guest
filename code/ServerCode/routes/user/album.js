const express = require('express')
const router = express.Router()
const albumStore = require('../../models/albumStore')

router.post('/getAlbum', async (req, res, next) => {
    res.status(200)
    res.type('application/json')
    
    let docs = await albumStore.findByUsername(req.body.username)

    res.send({
        'photos': docs
    })

}) 

router.post('/postAlbum', async (req, res, next) => {
    res.status(200)
    res.type('application/json')

    for (let i = 0; i < req.body.photos.length; i++) {
        await albumStore.createGraph(req.body.username, req.body.photos[i])
    }

    res.send({
        'status': true,
        'message': '照片上传成功'
    })
})

module.exports = router