const express = require('express')
const router = express.Router()
const userStore = require('../../models/userStore')
const blogStore = require('../../models/blogStore')
const blogGraphStore = require('../../models/blogGraphStore')
const userLikeBlogStore = require('../../models/userLikeBlogStore')
const userFollowStore = require('../../models/userFollowStore')

router.post('/releaseBlog', async (req, res, next) => {
    res.status(200)
    res.type('application/json')

    await blogStore.createBlog(req.body.username, req.body.content, req.body.dateTime)
    for (let i = 0; i < req.body.photos.length; i++) {
        await blogGraphStore.createGraph(req.body.username, req.body.content, req.body.photos[i])
    }

    res.send({
        'status': true,
        'message': '博客发布成功'
    })
})

router.post('/addOrDeleteLikeBlog', async (req, res, next) => {
    res.status(200)
    res.type('application/json')

    if (req.body.type == 'add') {
        await userLikeBlogStore.createLikeBlog(req.body.username, req.body.author, req.body.content)
    }
    else if (req.body.type == 'delete') {
        await userLikeBlogStore.deleteLikeBlog(req.body.username, req.body.author, req.body.content)
    }


    res.send({
        "status": true,
        "message": "更新成功"
    })
})

router.post('/getLikeBlog', async (req, res, next) => {
    res.status(200)
    res.type('application/json')

    let docs = await userLikeBlogStore.getLikeBlog(req.body.username)
 
    let blogs = []
    for (let i = 0; i < docs.length; i++) {
        let blog = new Object()
        blog.author = docs[i].author 
        blog.icon = await userStore.findByName(blog.author).icon 
        blog.content = docs[i].content
        let tempBlog = await blogStore.findBlog(blog.author, blog.content)
        blog.date = tempBlog.dateTime
        blog.lookNumber = tempBlog.lookNumber
        blog.likeNumber = tempBlog.likeNumber
        blog.commentNumber = tempBlog.commentNumber
        blog.photos = []
        let tempPhotos = await blogGraphStore.findGraph(blog.author, blog.content)
        for (let j = 0; j < tempPhotos.length; j++) {
            blog.photos.push(tempPhotos[j].graph)
        }
        blogs.push(blog)
    }

    res.send({
        "blogs": blogs
    })
})

router.post('/getFollowBlog', async (req, res, next) => {
    res.status(200)
    res.type('application/json')

    let docs = await userFollowStore.getFollow(req.body.username)
 
    let blogs = []
    for (let i = 0; i < docs.length; i++) {

        let tempBlog = await blogStore.findBlogByAuthor(docs[i].followUser)
        if (tempBlog == null)
            continue

        for (let j = 0; j < tempBlog.length; j++) {
            let blog = new Object()
            blog.author = docs[i].followUser 
            blog.icon = await userStore.findByName(blog.author).icon 
            blog.content = tempBlog[j].content
            blog.date = tempBlog[j].dateTime
            blog.lookNumber = tempBlog[j].lookNumber
            blog.likeNumber = tempBlog[j].likeNumber
            blog.commentNumber = tempBlog[j].commentNumber
            blog.photos = []
            let tempPhotos = await blogGraphStore.findGraph(blog.author, blog.content)
            for (let k = 0; k < tempPhotos.length; k++) {
                blog.photos.push(tempPhotos[k].graph)
            }
            blogs.push(blog)
        }
    }

    res.send({
        "blogs": blogs
    })
})

router.post('/getNewBlog', async (req, res, next) => {
    res.status(200)
    res.type('application/json')

    let docs = await blogStore.findBlogByDateTime()
 
    if (docs.length > 10) {
        docs = docs.slice(0, 9)
    }

    let blogs = []
    for (let i = 0; i < docs.length; i++) {
        let blog = new Object()
        blog.author = docs[i].author
        blog.icon = await userStore.findByName(blog.author).icon 
        blog.content = docs[i].content
        blog.date = docs[i].dateTime
        blog.lookNumber = docs[i].lookNumber
        blog.likeNumber = docs[i].likeNumber
        blog.commentNumber = docs[i].commentNumber
        blog.photos = []
        let tempPhotos = await blogGraphStore.findGraph(blog.author, blog.content)
        for (let j = 0; j < tempPhotos.length; j++) {
            blog.photos.push(tempPhotos[j].graph)
        }
        blogs.push(blog)
    }
    
    res.send({
        "blogs": blogs
    })
})

router.post('/getUserBlog', async (req, res, next) => {
    res.status(200)
    res.type('application/json')

    let docs = await blogStore.findBlogByAuthor(req.body.username)
 
    let blogs = []
    for (let i = 0; i < docs.length; i++) {
        let blog = new Object()
        blog.author = req.body.username
        blog.icon = await userStore.findByName(req.body.username).icon 
        blog.content = docs[i].content
        blog.date = docs[i].dateTime
        blog.lookNumber = docs[i].lookNumber
        blog.likeNumber = docs[i].likeNumber
        blog.commentNumber = docs[i].commentNumber
        blog.photos = []
        let tempPhotos = await blogGraphStore.findGraph(blog.author, blog.content)
        for (let j = 0; j < tempPhotos.length; j++) {
            blog.photos.push(tempPhotos[j].graph)
        }
        blogs.push(blog)
    }

    res.send({
        "blogs": blogs
    })
})


module.exports = router