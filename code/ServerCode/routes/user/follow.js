const express = require('express')
const router = express.Router()
const userFollowStore = require('../../models/userFollowStore')
const userStore = require('../../models/userStore')

router.post('/addOrDeleteFollow', async (req, res, next) => {
    res.status(200)
    res.type('application/json')

    if (req.body.type == 'add') {
        await userFollowStore.createFollow(req.body.username, req.body.name)
    }
    else if (req.body.type == 'delete') {
        await userFollowStore.deleteFollow(req.body.username, req.body.name)
    }

    res.send({
        'status': true,
        'message': '更新成功'
    })
})

router.post('/getFollow', async (req, res, next) => {
    res.status(200)
    res.type('application/json')

    let follows = []

    let docs = await userFollowStore.getFollow(req.body.username)
    for (let i = 0; i < docs.length; i++) {
        let follow = new Object()
        follow.name = docs[i].followUser
        follow.icon = userStore.findByName(docs[i].followUser)
        follows.push(follow)
    }

    res.send({
        'follows': follows
    })
})

router.post('/isFollow', async (req, res, next) => {
    res.status(200)
    res.type('application/json')

    let docs = await userFollowStore.getFollow(req.body.username)
    let isFollow = false
    
    for (let i = 0; i < docs.length; i++) {
        if (docs[i].followUser == req.body.name) {
            isFollow = true
            break
        }
    }

    res.send({
        'isFollow': isFollow
    })
})

module.exports = router