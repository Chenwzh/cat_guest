const express = require('express')
const router = express.Router()
const userStore = require('../../models/userStore')

router.get('/', async (req, res, next) => {
    res.status(200)
    res.type('application/json')
    
    let docs = await userStore.findByUsername(req.body.username)

    res.send({
        'name': docs.name,
        'description': docs.description,
        'icon': docs.icon
    })

}) 

router.post('/', async (req, res, next) => {
    res.status(200)
    res.type('application/json')

    let result = new Object()

    let docs = await userStore.findByName(req.body.name)
    if (docs == null) {
        await userStore.updateInfo(req.body.username, req.body.name, req.body.description, req.body.icon)
        result.status = true
        result.message = '更新成功'
    }
    else if (docs.username != req.body.username) {
        result.status = false 
        result.message = '昵称已存在'
    }
    else {
        await userStore.updateInfo(req.body.username, req.body.name, req.body.description, req.body.icon)
        result.status = true
        result.message = '更新成功'
    }
    

    res.send(result)
})

module.exports = router