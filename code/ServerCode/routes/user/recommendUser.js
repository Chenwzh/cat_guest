const express = require('express')
const router = express.Router()
const userStore = require('../../models/userStore')

router.post('/', async (req, res, next) => {
    res.status(200)
    res.type('application/json')
    
    let userNums = await userStore.findAllUserNum()
    let docs = await userStore.findEightUser(req.body.username)
    if (userNums - 1 >= 8) {
        let n = Math.floor(Math.random() * (userNums - 9))
        docs = docs.slice(n, n + 8)
    }
    res.send({
        'users': docs
    })
})

module.exports = router