const express = require('express')
const router = express.Router()
const userStore = require('../models/userStore')

router.post('/', async (req, res, next) => {
    res.status(200)
    res.type('application/json')

    let docs = await userStore.findByUsernameAndPassword(req.body.username, req.body.password)
    if (!docs) {
        res.send({
            'status': false,
            'message': '用户名、密码错误'          
        })
        return
    }

    res.send({
        'status': true,
        'message': '登录成功',
        'name': docs.name,
        'icon': docs.icon,
        'description': docs.description
    })
})

module.exports = router