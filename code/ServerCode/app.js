const express = require('express')
const app = express()
const path = require('path')
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const session = require('express-session')
const MongoStore = require('connect-mongo')(session)
const mongoose = require('mongoose')
const regist = require('./routes/regist')
const signin = require('./routes/signin')
const personalData = require('./routes/user/personalData')
const album = require('./routes/user/album')
const blog = require('./routes/user/blog')
const recommendUser = require('./routes/user/recommendUser')
const follow = require('./routes/user/follow')

app.use(bodyParser.json({limit: '50mb'}))
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}))

app.use(cookieParser())

app.use(session({
  secret: 'secret',
  cookie: {maxAge: 60 * 1000 * 30},
  resave: false,
  saveUninitialized: true,
  store: new MongoStore({   
    mongooseConnection: mongoose.connection 
  })
}))

app.use('/regist', regist)
app.use('/signin', signin)
app.use('/personalData', personalData)
app.use('/album', album)
app.use('/blog', blog)
app.use('/recommendUser', recommendUser)
app.use('/follow', follow)

module.exports = app