package com.example.watchcat2k.finalproject;

import java.io.Serializable;
import java.util.ArrayList;

public class find_users_packet implements Serializable {
    private ArrayList<users> users;


    public ArrayList<users> getAll_users() {
        return users;
    }

    public void setAll_users(ArrayList<users> all_users) {
        this.users = all_users;
    }



    public class users implements Serializable {
        private String username;
        private String name;
        private String description;
        private String icon;

        public users(String username, String name, String description, String icon){
            this.username = username;
            this.name = name;
            this.description = description;
            this.icon = icon;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void setIcon(String icon) {
            this.icon = icon;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getDescription() {
            return description;
        }

        public String getName() {
            return name;
        }

        public String getIcon() {
            return icon;
        }

        public String getUsername() {
            return username;
        }

    }
}
