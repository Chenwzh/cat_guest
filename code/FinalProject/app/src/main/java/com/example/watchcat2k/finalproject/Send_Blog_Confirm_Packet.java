package com.example.watchcat2k.finalproject;

import java.io.Serializable;

public class Send_Blog_Confirm_Packet implements Serializable {
    private boolean status;
    private String message;

    public Send_Blog_Confirm_Packet(boolean status, String message){
        this.status = status;
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
