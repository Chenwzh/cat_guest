package com.example.watchcat2k.finalproject;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class BlogAdapter extends ArrayAdapter<User_Info_Activity.User_All_Blogs.blog> {

    private int resourceId;

    public BlogAdapter(Context context, int textViewResourceId, List<User_Info_Activity.User_All_Blogs.blog> objects) {
        super(context, textViewResourceId, objects);
        resourceId = textViewResourceId;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        User_Info_Activity.User_All_Blogs.blog tmp = getItem(position);
        View view;
        ViewHolder viewHolder;
        viewHolder = new ViewHolder();
        viewHolder.pictures = new ImageView[6];
        if (convertView == null) {
            view = LayoutInflater.from(parent.getContext()).inflate(resourceId, parent, false);

            viewHolder.icon = (ImageView) view.findViewById(R.id.imageView_item_blog_image);
            viewHolder.username = (TextView) view.findViewById(R.id.textView_item_blog_username);
            viewHolder.time = (TextView) view.findViewById(R.id.textView_item_blog_time);
            viewHolder.content = (TextView) view.findViewById(R.id.textView_item_blog_content);
            viewHolder.pictures[0] = (ImageView) view.findViewById(R.id.imageView_item_blog_picture1);
            viewHolder.pictures[1] = (ImageView) view.findViewById(R.id.imageView_item_blog_picture2);
            viewHolder.pictures[2] = (ImageView) view.findViewById(R.id.imageView_item_blog_picture3);
            viewHolder.pictures[3] = (ImageView) view.findViewById(R.id.imageView_item_blog_picture4);
            viewHolder.pictures[4] = (ImageView) view.findViewById(R.id.imageView_item_blog_picture5);
            viewHolder.pictures[5] = (ImageView) view.findViewById(R.id.imageView_item_blog_picture6);
            viewHolder.viewCount = (TextView) view.findViewById(R.id.textView_item_blog_view_count);
            viewHolder.transmitCount = (TextView) view.findViewById(R.id.textView_item_blog_transmit_count);
            viewHolder.commentCount = (TextView) view.findViewById(R.id.textView_item_blog_comment_count);
            viewHolder.likeCount = (TextView) view.findViewById(R.id.textView_item_blog_like_count);
            view.setTag(viewHolder);
        } else {
            view = convertView;
            viewHolder = (ViewHolder) view.getTag();
        }

        if (tmp.getIcon() != null) {
            viewHolder.icon.setImageBitmap(base64_to_Bitmap(tmp.getIcon()));
        }

        viewHolder.username.setText(tmp.getAuthor());
        viewHolder.time.setText(tmp.getDate());
        viewHolder.content.setText(tmp.getContent());
        for (int i = 0; i < tmp.getPhotos().size(); i++) {
            viewHolder.pictures[i].setImageBitmap(base64_to_Bitmap(tmp.getPhotos().get(i)));
        }
        if(tmp.getPhotos().size() == 0){//无照片
            viewHolder.pictures[0].setVisibility(View.GONE);
            viewHolder.pictures[1].setVisibility(View.GONE);
            viewHolder.pictures[2].setVisibility(View.GONE);
            viewHolder.pictures[3].setVisibility(View.GONE);
            viewHolder.pictures[4].setVisibility(View.GONE);
            viewHolder.pictures[5].setVisibility(View.GONE);
        }else if(tmp.getPhotos().size() == 1){//一张照片
            viewHolder.pictures[1].setVisibility(View.INVISIBLE);
            viewHolder.pictures[2].setVisibility(View.INVISIBLE);
            viewHolder.pictures[3].setVisibility(View.GONE);
            viewHolder.pictures[4].setVisibility(View.GONE);
            viewHolder.pictures[5].setVisibility(View.GONE);
        }else if(tmp.getPhotos().size() == 2){
            viewHolder.pictures[2].setVisibility(View.INVISIBLE);
            viewHolder.pictures[3].setVisibility(View.GONE);
            viewHolder.pictures[4].setVisibility(View.GONE);
            viewHolder.pictures[5].setVisibility(View.GONE);
        }else if(tmp.getPhotos().size() == 3){
            viewHolder.pictures[3].setVisibility(View.GONE);
            viewHolder.pictures[4].setVisibility(View.GONE);
            viewHolder.pictures[5].setVisibility(View.GONE);
        }else if(tmp.getPhotos().size() == 4){
            viewHolder.pictures[4].setVisibility(View.INVISIBLE);
            viewHolder.pictures[5].setVisibility(View.INVISIBLE);
        }else if(tmp.getPhotos().size() == 5){
            viewHolder.pictures[5].setVisibility(View.INVISIBLE);
        }
        viewHolder.viewCount.setText(String.valueOf(tmp.getLookNumber()));
        viewHolder.transmitCount.setText(String.valueOf(0));
        viewHolder.commentCount.setText(String.valueOf(tmp.getCommentNumber()));
        viewHolder.likeCount.setText(String.valueOf(tmp.getLikeNumber()));
        return view;
    }

    class ViewHolder {
        ImageView icon;
        TextView username;
        TextView time;
        TextView content;
        ImageView[] pictures;
        TextView viewCount;
        TextView transmitCount;
        TextView commentCount;
        TextView likeCount;
    }

    private Bitmap base64_to_Bitmap(String icon){
        byte[] bytes = Base64.decode(icon, Base64.DEFAULT);
        Bitmap bm = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
        return bm;
    }
}
