package com.example.watchcat2k.finalproject;
import java.io.Serializable;
import java.util.ArrayList;

public class Blog implements Serializable {
    private String username;
    private String icon;
    private String content;
    private String date;
    private int lookCount;
    private int likeCount;
    private int commentCount;
    private ArrayList<String> photos;

    Blog(String username, String icon, String content, String date, int lookCount, int likeCount, int commentCount, ArrayList<String> photos) {
        this.username = username;
        this.icon = icon;
        this.content = content;
        this.date = date;
        this.lookCount = lookCount;
        this.likeCount = likeCount;
        this.commentCount = commentCount;
        this.photos = photos;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String usrename) {
        this.username = usrename;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getLookCount() {
        return lookCount;
    }

    public void setLookCount(int lookCount) {
        this.lookCount = lookCount;
    }

    public int getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(int likeCount) {
        this.likeCount = likeCount;
    }

    public int getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(int commentCount) {
        this.commentCount = commentCount;
    }

    public ArrayList<String> getPhotos() {
        return photos;
    }

    public void setPhotos(ArrayList<String> photos) {
        this.photos = photos;
    }
}
