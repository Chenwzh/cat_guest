package com.example.watchcat2k.finalproject;

import java.io.Serializable;

public class change_my_info_packet implements Serializable {
    private String username;
    private String name;
    private String description;
    private String icon;

    public change_my_info_packet(String user, String name, String description, String icon){
        this.username = user;
        this.name = name;
        this.description = description;
        this.icon = icon;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getDescription() {
        return description;
    }

    public String getName() {
        return name;
    }

    public String getIcon() {
        return icon;
    }

}
