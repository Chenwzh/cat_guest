package com.example.watchcat2k.finalproject;

import java.io.Serializable;

public class find_user implements Serializable {
    private String username;
    private String name;
    private String description;
    private String icon;

    public find_user(String username, String name, String description, String icon){
        this.username = username;
        this.name = name;
        this.description = description;
        this.icon = icon;

    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIcon() {
        return icon;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

}

