package com.example.watchcat2k.finalproject;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

public class NewFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_new, container, false);

        BlogAdapter adapter = new BlogAdapter(getContext(), R.layout.item_blog, Blog_Data.newBlogs);
        ((ListView)view.findViewById(R.id.listView_new_blog)).setAdapter(adapter);
        return view;
    }


}
