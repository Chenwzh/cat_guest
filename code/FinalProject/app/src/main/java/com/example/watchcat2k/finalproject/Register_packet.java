package com.example.watchcat2k.finalproject;

import java.io.Serializable;

public class Register_packet implements Serializable {
    String name;
    String username;
    String password;
    String icon;

    Register_packet(String name, String username, String password, String icon){
        this.name = name;
        this.username = username;
        this.password = password;
        this.icon = icon;
    }
    public void setName(String name) {
        this.name = name;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

    public String getIcon() {
        return icon;
    }

    public String getUsername() {
        return username;
    }
}
