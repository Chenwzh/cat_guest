package com.example.watchcat2k.finalproject;

import java.io.Serializable;
import java.util.ArrayList;

public class Get_Blog_Confirm_Packet implements Serializable {
    private boolean status;
    private String message;
    private ArrayList<Blog> blogs;

    public Get_Blog_Confirm_Packet(boolean status, String message, ArrayList<Blog> blogs){
        this.status = status;
        this.message = message;
        this.blogs = blogs;
    }

    public boolean isStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<Blog> getBlogs() {
        return blogs;
    }

    public void setBlogs(ArrayList<Blog> blogs) {
        this.blogs = blogs;
    }
}
