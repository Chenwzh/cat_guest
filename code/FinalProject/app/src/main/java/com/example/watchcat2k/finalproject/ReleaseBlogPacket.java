package com.example.watchcat2k.finalproject;

import java.io.Serializable;
import java.util.ArrayList;

public class ReleaseBlogPacket implements Serializable {
    private String username;
    private String content;
    private String dateTime;
    private ArrayList<String>photos;

    public ReleaseBlogPacket(String username, String content, String dateTime, ArrayList<String>photos){
        this.username = username;
        this.content = content;
        this.dateTime = dateTime;
        this.photos = photos;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }


    public void setContent(String content) {
        this.content = content;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public void setPhotos(ArrayList<String> photos) {
        this.photos = photos;
    }

    public String getContent() {
        return content;
    }

    public ArrayList<String> getPhotos() {
        return photos;
    }

    public String getDateTime() {
        return dateTime;
    }
}
