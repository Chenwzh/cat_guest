package com.example.watchcat2k.finalproject;

import java.io.Serializable;

public class Confirm_packet implements Serializable{
    Boolean status;
    String message;

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public Boolean getStatus() {
        return status;
    }
}
