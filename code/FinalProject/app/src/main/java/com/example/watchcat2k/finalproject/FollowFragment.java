package com.example.watchcat2k.finalproject;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

public class FollowFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_follow, container, false);

        BlogAdapter adapter = new BlogAdapter(getContext(), R.layout.item_blog, Blog_Data.followBlogs);
        ((ListView)view.findViewById(R.id.listView_follow_blog)).setAdapter(adapter);

        return view;
    }



}
