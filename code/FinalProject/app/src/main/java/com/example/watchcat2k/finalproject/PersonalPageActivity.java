package com.example.watchcat2k.finalproject;

import android.graphics.Color;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class PersonalPageActivity extends AppCompatActivity {
    private RadioGroup radioGroup_personal_page_radioGroup;
    private RadioButton radioButton_personal_page_blog;
    private RadioButton radioButton_personal_page_picture;
    private RadioButton radioButton_personal_page_follow;

    private FragmentManager fragmentManager = getSupportFragmentManager();
    private PersonalPageBlogFragment personalPageBlogFragment = new PersonalPageBlogFragment();
    private PersonalPagePictureFragment personalPagePictureFragment = new PersonalPagePictureFragment();
    private PersonalPageFollowFragment personalPageFollowFragment = new PersonalPageFollowFragment();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_page);

        radioGroup_personal_page_radioGroup = (RadioGroup) findViewById(R.id.radioGroup_personal_page_radioGroup);
        radioButton_personal_page_blog = (RadioButton) findViewById(R.id.radioButton_personal_page_blog);
        radioButton_personal_page_picture = (RadioButton) findViewById(R.id.radioButton_personal_page_picture);
        radioButton_personal_page_follow = (RadioButton) findViewById(R.id.radioButton_personal_page_follow);

        //设置初始帧
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.constraintLayout_personal_page_fragment, personalPageBlogFragment);
        fragmentTransaction.commit();

        radioGroup_personal_page_radioGroup.setOnCheckedChangeListener(new MyRadioGroupCheckChangeListener());

    }

    private class MyRadioGroupCheckChangeListener implements RadioGroup.OnCheckedChangeListener {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            switch (checkedId) {
                case R.id.radioButton_personal_page_blog: {
                    fragmentTransaction.replace(R.id.constraintLayout_personal_page_fragment, personalPageBlogFragment);
                    radioButton_personal_page_blog.setTextColor(Color.parseColor("#4c88a3"));
                    radioButton_personal_page_picture.setTextColor(Color.parseColor("#8A000000"));
                    radioButton_personal_page_follow.setTextColor(Color.parseColor("#8A000000"));
                    break;
                }
                case R.id.radioButton_personal_page_picture: {
                    fragmentTransaction.replace(R.id.constraintLayout_personal_page_fragment, personalPagePictureFragment);
                    radioButton_personal_page_blog.setTextColor(Color.parseColor("#8A000000"));
                    radioButton_personal_page_picture.setTextColor(Color.parseColor("#4c88a3"));
                    radioButton_personal_page_follow.setTextColor(Color.parseColor("#8A000000"));
                    break;
                }
                case R.id.radioButton_personal_page_follow: {
                    fragmentTransaction.replace(R.id.constraintLayout_personal_page_fragment, personalPageFollowFragment);
                    radioButton_personal_page_blog.setTextColor(Color.parseColor("#8A000000"));
                    radioButton_personal_page_picture.setTextColor(Color.parseColor("#8A000000"));
                    radioButton_personal_page_follow.setTextColor(Color.parseColor("#4c88a3"));
                    break;
                }
            }
            fragmentTransaction.commit();
        }
    }

}
