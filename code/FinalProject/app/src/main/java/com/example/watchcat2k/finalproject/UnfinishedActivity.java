package com.example.watchcat2k.finalproject;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class UnfinishedActivity extends AppCompatActivity {
    private ImageView imageView_unfinished_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unfinished);

        imageView_unfinished_back = (ImageView) findViewById(R.id.imageView_unfinished_back);

        imageView_unfinished_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UnfinishedActivity.this.finish();
            }
        });
    }
}
