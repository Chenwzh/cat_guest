package com.example.watchcat2k.finalproject;

import java.io.Serializable;

public class Login_packet implements Serializable {
    String username;
    String password;

    Login_packet(String username, String password){
        this.password = password;
        this.username = username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
