

package com.example.watchcat2k.finalproject;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class BeginActivity extends AppCompatActivity {
    private TextView textView_login_app_name;
    Button register;
    Button login;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_begin);

        textView_login_app_name = (TextView) findViewById(R.id.textView_login_app_name);


        //设置字体
        Typeface typeface = ResourcesCompat.getFont(this, R.font.huamao);
        textView_login_app_name.setTypeface(typeface);

        register = (Button)findViewById(R.id.register_btn);
        login = (Button)findViewById(R.id.login_btn);

        set_click();
    }

    void set_click(){
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BeginActivity.this, RegisterUserActivity.class);
                startActivity(intent);
            }
        });

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BeginActivity.this, LoginUserActivity.class);
                startActivity(intent);
            }
        });
    }

}
