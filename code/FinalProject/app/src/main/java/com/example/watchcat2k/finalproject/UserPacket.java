package com.example.watchcat2k.finalproject;

import java.io.Serializable;

public class UserPacket implements Serializable {
    private String username;

    public UserPacket(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
