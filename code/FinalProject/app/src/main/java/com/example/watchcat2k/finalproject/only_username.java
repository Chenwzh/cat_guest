package com.example.watchcat2k.finalproject;

import java.io.Serializable;

public class only_username implements Serializable {
    private String username;

    public only_username(String username){
        this.username = username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }
}
