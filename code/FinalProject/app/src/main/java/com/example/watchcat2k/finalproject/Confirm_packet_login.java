package com.example.watchcat2k.finalproject;

import java.io.Serializable;

public class Confirm_packet_login implements Serializable {
    /*status: bool, 成功true, 失败false
    message: string, 出错信息
    name: string, 昵称
    icon: string, 头像的base64编码
    description: string, 个人描述
    */

    private Boolean status;
    private String message;
    private String name;
    private String icon;
    private String description;

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIcon() {
        return icon;
    }

    public String getName() {
        return name;
    }

    public Boolean getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public String getDescription() {
        return description;
    }
}
