package com.example.watchcat2k.finalproject;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import de.hdodenhof.circleimageview.CircleImageView;

public class PersonFragment extends Fragment {
    private CircleImageView circleImageView;
    private TextView textView_me_username;
    private TextView textView_me_summary;
    private TextView textView_me_name;
    private User_basic_info my_info;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_me, container, false);

        circleImageView = view.findViewById(R.id.circleImageView_me_image);
        textView_me_summary = view.findViewById(R.id.textView_me_summary);
        textView_me_username = view.findViewById(R.id.textView_me_username);
        textView_me_name = view.findViewById(R.id.textView_me_name);


        //获取activity传递过来的bundle
        Bundle bundle = getArguments();
        my_info = (User_basic_info) bundle.get("user");

        //初始化用户信息
        textView_me_username.setText(my_info.getUsername());
        textView_me_summary.setText(my_info.getDescription());
        textView_me_name.setText(my_info.getName()+"（昵称）");
        Bitmap bm = null;
        byte[] bytes = Base64.decode(my_info.getIcon(), Base64.DEFAULT);
        bm = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
        circleImageView.setImageBitmap(bm);


        return view;
    }



}
