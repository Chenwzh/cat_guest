package com.example.watchcat2k.finalproject;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class HomeFragment extends Fragment {
    private RadioGroup radioGroup_home;
    private RadioButton radioButton_home_like;
    private RadioButton radioButton_home_find;
    private RadioButton radioButton_home_new;
    private FragmentManager fragmentManager;
    private FollowFragment followFragment = new FollowFragment();
    private FindFragment findFragment = new FindFragment();
    private NewFragment newFragment = new NewFragment();
    private User_basic_info my_info;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        radioGroup_home = view.findViewById(R.id.radioGroup_home);
        radioButton_home_like = view.findViewById(R.id.radioButton_home_like);
        radioButton_home_find = view.findViewById(R.id.radioButton_home_find);
        radioButton_home_new = view.findViewById(R.id.radioButton_home_new);
        fragmentManager = getChildFragmentManager();



        //设置初始帧
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.fragmentContainer_inner, followFragment);
        fragmentTransaction.commit();



        radioGroup_home.setOnCheckedChangeListener(new MyRadioGroupClick());

        //获取activity传递过来的bundle

        //获取activity传递过来的bundle
        Bundle bundle = getArguments();
        my_info = (User_basic_info) bundle.get("user");


        return view;
    }

    private class MyRadioGroupClick implements RadioGroup.OnCheckedChangeListener {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            switch (checkedId) {
                case R.id.radioButton_home_like: {
                    fragmentTransaction.replace(R.id.fragmentContainer_inner, followFragment);
                    radioButton_home_like.setTextColor(Color.parseColor("#4c88a3"));
                    radioButton_home_find.setTextColor(Color.parseColor("#8A000000"));
                    radioButton_home_new.setTextColor(Color.parseColor("#8A000000"));
                    break;
                }
                case R.id.radioButton_home_find: {

                    Bundle bundle = new Bundle();
                    bundle.putSerializable("user", my_info);
                    findFragment.setArguments(bundle);


                    fragmentTransaction.replace(R.id.fragmentContainer_inner, findFragment);
                    radioButton_home_like.setTextColor(Color.parseColor("#8A000000"));
                    radioButton_home_find.setTextColor(Color.parseColor("#4c88a3"));
                    radioButton_home_new.setTextColor(Color.parseColor("#8A000000"));
                    break;
                }
                case R.id.radioButton_home_new: {
                    fragmentTransaction.replace(R.id.fragmentContainer_inner, newFragment);
                    radioButton_home_like.setTextColor(Color.parseColor("#8A000000"));
                    radioButton_home_find.setTextColor(Color.parseColor("#8A000000"));
                    radioButton_home_new.setTextColor(Color.parseColor("#4c88a3"));
                    break;
                }
            }
            fragmentTransaction.commit();
        }
    }

}
