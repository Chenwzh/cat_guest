package com.example.watchcat2k.finalproject;

import java.io.Serializable;

public class user1_follow_user2_packet implements Serializable {
    private String username;
    private String name;

    public user1_follow_user2_packet(String username, String name) {
        this.username = username;
        this.name = name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public String getUsername() {
        return username;
    }
}
